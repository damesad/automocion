<h1>Plugin "Automoción"</h1>

<strong>Ubicación del plugin:</strong> facturascripts/plugins/automoción

<br>

<strong>Descripción</strong>

Plugin para añadir soporte para el sector de la automoción en FacturaScripts

<br>

<strong>Características:</strong>

<ul>
   <li>Marcas: permite insertar/editar marcas nuevas de vehículos.</li>
   <li>Modelos: permite insertar/editar modelos nuevos de vehículos.</li>
   <li>Vehículos: permite insertar/editar las fichas técnicas de los vehíclos relacionadas con los clientes.</li>
   <li>Inspecciones vehículos: permite realizar una inspección visual de estado del vehículo.</li>
   <li>Añadida búsqueda a los listados</li>
   <li>Añadida paginación en los listados y en las búsquedas</li>
   <li>Vehículos e Inspecciones a vehículos accesible desde la página de cliente</li>
   <li>Añadido soporte para relacionar presupuestos, pedidos, servicios, albaranes y facturas de ventas con vehículos</li>
   <li>Añadido soporte para relacionar pedidos, albaranes y facturas de compras con vehículos (Petición desde la comunidad: https://www.facturascripts.com/comm3/index.php?page=community_item&id=3021)</li>
   <li>Añadido soporte para localizar desde la página del vehículo todos sus documentos relacionados (presupuestos, pedidos, servicios, albaranes y facturas)</li>
   <li>Añadido soporte para clickableRow, para mejorar la experiencia en dispositivos táctiles</li>
</ul>

<br>

<strong>Sugerencias en consideración:</strong>

<ul>
   <li>
	Pendiente recopilar más detalles: Control de marcas por WMIs, para identificar desde el VIN la marca y modelo.
	https://es.wikipedia.org/wiki/N%C3%BAmero_de_chasis
	https://en.wikibooks.org/wiki/Vehicle_Identification_Numbers_(VIN_codes)/World_Manufacturer_Identifier_(WMI)#List_of_all_WMIs
	http://www.analogx.com/contents/vinview.htm
	https://www.autodna.com/
	http://es.vin-info.com/
	http://www.vin-info.com/
	http://www.vindecoder.net/
	http://www.vindecoderz.com/
	
   </li>
</ul>

<br>

<strong>Apoya el plugin:</strong>

Puedes apoyar el desarrollo del plugin sugiriendo y enviando cambios.
